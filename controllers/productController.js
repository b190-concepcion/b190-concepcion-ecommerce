const Product = require("../models/Product.js");

// Creates new product
/*
		- create a new Course object using the mongoose model and the information from the request body {name, description, price}
		- save the new Course object to the database
*/
module.exports.addProduct = (data) => {
		let newProduct = new Product({
			name : data.name,
			platform : data.platform,
			description : data.description,
			price : data.price,
			stocks: data.stocks
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return `You must fill-in all of the fields`;
			} else {
				return `${newProduct.name} with a Product ID: ${newProduct._id} is successfully added`;
			};
		});
};

// retrieving all active products
/*
	retrieve all product from databases with the property of "isActive" to true
*/
module.exports.getAllActiveProducts = () =>{
	return Product.find({ isActive: true }).then(result =>{
		return result;
	})
};

// retrieving a specific product
/*
	Retrieve the product that match the procut Id provided in the URL

*/
module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};

// update a product

module.exports.updateProduct = ( reqParams, reqBody ) => {
	let updateProduct = {
		name: reqBody.name,
		platform: reqBody.platform,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}
	// findByIdAndUpdate - its purpose is to find a specific id in the database (first parameter) and update it using the information coming from the request body (second parameter)
	/*
		findByIdAndUpdate(documentId, updatesToBeApplied)
	*/
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, err)=>{
		if (err) {
			return `Item cannot be found`;
		}else{
			return `Updated Successfully`;
		}
	})
};

// Archive a product
/*
	1. create an object "updateActiveField" and store isActive: false inside
	2. Find and update the product using the productId found in the request params and the variable "updateActiveField" 
	3. return false if there are errors, true if the updating is successful
*/
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return `Item cannot be found`;
		} else {
			return `Archive product successfully`;
		}
	});
};