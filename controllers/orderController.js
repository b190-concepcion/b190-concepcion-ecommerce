const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");


//Create orders
module.exports.addOrder = async (data, requestBody) =>{

	let isOrderUpdated = await Product.findById(data.productId).then((result) =>{
		let newOrder = new Order({
			products : {
				userId: data.userId,
				productId: data.productId,
				quantity: requestBody.quantity
			},
			totalAmount: result.price*requestBody.quantity
		});
		if(result.stocks == null || result.stocks < requestBody.quantity){

		}else{
			return newOrder.save().then((order, error)=>{
				if(error){
					return false;
				}else{
					return true;
				}
			})
		}
	})
	
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		if(product.stocks == null || product.stocks < requestBody.quantity){

		}else{
			product.stocks-=requestBody.quantity
		// saves the updated course information in the database
		return product.save().then((order, error)=>{
			if (error) {
				return false;
			}else{
				return true;
			}
		})
	}
});

	if(isOrderUpdated && isProductUpdated){
		return `Order Successfully`; 

	}else{
		return `Invalid Inputs or Out of Stocks`
	}
}


//Retrieve authenticated user order

module.exports.getUserOrder = (data) => {
	console.log(data.userId)
	return Order.find({products:{$elemMatch:{userId: data.userId}}})
	.populate("products.userId", "-email -password -__v -isAdmin")
	.populate("products.productId", "-isActive  -createdOn -__v -description -stocks")
	
	.then((result) =>{
		return result;
	})
}

//Retrieve all orders
module.exports.getAllOrders = () =>{
	return Order.find({  })
	.populate("products.userId", "-email -password -__v -isAdmin")
	.populate("products.productId", "-isActive  -createdOn -__v -description -stocks")
	.then(result =>{
		console.log(result)
		return result;
	})
};
