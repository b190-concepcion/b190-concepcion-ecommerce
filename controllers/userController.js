const User = require("../models/User.js");
const Order = require("../models/Order.js");

const auth = require("../auth.js");

const bcrypt = require("bcrypt");


// User Registration
/*
	1. create a new User object using the mongoose mode and the information from the request body
	2. Make sure that the password is encrypted
	3. save the object to the database
	*/

	module.exports.registerUser = (reqBody) => {
		return User.findOne( { email: reqBody.email } ).then(result =>{
		// if the user email does not exist
		console.log(reqBody)
		if(result !== null){
			return `Email already exist`
		}else if(reqBody.firstName === "" || reqBody.lastName === "" || reqBody.mobileNo === "" || reqBody.password === ""){
			return `You must fill-in all of the fields`
		}else{
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: bcrypt.hashSync(reqBody.password, 10)
			})
			return newUser.save().then((user, error) =>{
				if (error) {
					return `You must fill-in all of the fields`;
				}else{
					return `Hi ${user.firstName}!!, You are successfully registered`;
				}
			})
		}
	})
	}


// User Login / User Aunthentication
/*
	1. check the database of the user email exists
	2. compare the password provided in the request body with the password stored in the database
	3. generate/return a JSON web token if the user hasd successfully logged in and return false if not
	*/
	module.exports.loginAuthenticateUser = ( reqBody ) => {
		return User.findOne( { email: reqBody.email } ).then(result =>{
		// if the user email does not exist
		if(result === null){
			return `The user email does not exist`;
		// if the user email exists in the database
	} else {
			// compareSync = decodes the encrypted password from the database and compares it to the password received from the request body
			// it's a good that if the value returned by a method/function is boolean, the variable name should be answerable by yes/no
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return `Invalid Password`;
			}
		}
	} )
	} 


// Retrieve user details
	/*
		Steps:
		1. Find the document in the database using the user's ID
		2. Reassign the password of the returned document to an empty string
		3. Return the result back to the frontend
		*/
		module.exports.getProfile = () => {
			return User.find({ }).then(result => {
				for(let i = 0; i < result.length; i++  ){
				result[i].password =  ""
			}
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		//result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
		};

//Route for setting User as Admin


module.exports.updateUser = ( reqParams, reqBody ) => {
	let updateUser = {
		isAdmin: true
	}
	// findByIdAndUpdate - its purpose is to find a specific id in the database (first parameter) and update it using the information coming from the request body (second parameter)
	/*
		findByIdAndUpdate(documentId, updatesToBeApplied)
		*/
		return User.findByIdAndUpdate(reqParams.userId, updateUser).then((user, err)=>{
			if (err) {
				return `ID cannot be found`;
			}else{
				return `Successfully becomes an administrator`;
			}
		})
	};