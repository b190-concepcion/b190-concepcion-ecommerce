
const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({
	products: [
	{
		userId:{
		type: mongoose.Schema.Types.ObjectId,
        ref: "User"
		},
		productId:{
			type: mongoose.Schema.Types.ObjectId,
       		 ref: "Product"
		},
			quantity:{
			type: Number,
			required: [true, "Quantity is required"],
		}
	}
	],
	totalAmount: {
		type: Number,
		required: [true, "Amount is required"],
	},
	purchasedOn:{
		type: Date,
		// creates a new "date" that stores the current date and time of the course creation
		default: new Date()
	}	
});


module.exports = mongoose.model("Order",orderSchema);

