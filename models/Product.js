const mongoose = require("mongoose");


const productSchema = new mongoose.Schema({
	name:{
		type: String,
		// requires the data for this field to be included when creating a document/record
		required: [true, "Course name is required"]
	},
	platform: {
		type: String,
		required: [true, "platform is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		// creates a new "date" that stores the current date and time of the course creation
		default: new Date()
	}
});


module.exports = mongoose.model("Product",productSchema);