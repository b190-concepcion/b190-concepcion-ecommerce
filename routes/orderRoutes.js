const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");


// Route for creating order (Non-Admin)

router.post("/checkout", auth.verify,(req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(`Access Denied! Only Users Can Create Orders!`);
	} else {
		let data = {
		// User ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}
	console.log(data)
	orderController.addOrder(data,req.body).then(resultFromController => res.send(resultFromController));
}
});

// route for getting Authenticated user's order
router.get('/myOrders', (req, res) =>{
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(`Access Denied! Only Users Can Create Orders!`);
	} else {
	
	let data = {
		// User ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,
	}
	orderController.getUserOrder(data).then(resultFromController => res.send(resultFromController));
}
});



// route for retrieving a all orders
router.get('/orders', auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(`Access Denied! You are not an administrator`);
	} else {
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
	}
});

module.exports = router;