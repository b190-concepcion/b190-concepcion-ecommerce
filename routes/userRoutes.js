const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


// route for user registration
/*
	pass the req.body that has the contents of the user credentials to be saved in our database
	 call the function "registerUser" from our userController
	 */
	 router.post("/register", ( req, res ) => {
	 	userController.registerUser( req.body ).then(resultFromController => res.send(resultFromController));
	 } );


// route for user authentication
router.post("/login", ( req, res ) => {
	userController.loginAuthenticateUser( req.body ).then( resultFromController => res.send( resultFromController ) );
} );

// Route for retrieving user details (ADMIN)

router.get("/details", auth.verify, (req, res) => {
	// 
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(`Access Denied! You are not an administrator`);
	} else {
		userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
	}
});
//Route for setting user as admin

router.put('/:userId/setAsAdmin', auth.verify, (req, res) =>{
	// 
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(`Access Denied! You are not an administrator`);
	} else {
		userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
} );

module.exports = router;