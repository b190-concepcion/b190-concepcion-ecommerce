const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Route for creating a product

router.post("/", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(`Access Denied! You are not an administrator`);
	} else {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
});

// route for getting all product
router.get('/all', (req, res)=>{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// route for getting all ACTIVE product
router.get('/', (req, res) =>{
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
} );

// route for retrieving a specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// route for updating a product
router.put('/:productId', auth.verify, (req, res) =>{
		if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(`Access Denied! You are not an administrator`);
	} else {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
}
} );

// route for archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(`Access Denied! You are not an administrator`);
	} else {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	}
});

module.exports = router;